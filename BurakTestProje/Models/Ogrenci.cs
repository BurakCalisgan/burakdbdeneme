﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BurakTestProje.Models
{
    public class Ogrenci
    {
        
        public int Id { get; set; }
        [Display(Name = "Öğrenci Adı")]
        public string OgrenciAdi { get; set; }
        [Display(Name = "Öğrenci Soyadı")]
        public string OgrenciSoyadi { get; set; }
        [Display(Name = "Okuduğu Yıl Sayısı")]
        public int YilSayisi { get; set; }

    }
}