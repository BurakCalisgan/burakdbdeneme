﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BurakTestProje.Startup))]
namespace BurakTestProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
