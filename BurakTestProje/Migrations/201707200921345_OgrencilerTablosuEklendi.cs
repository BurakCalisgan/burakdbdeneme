namespace BurakTestProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OgrencilerTablosuEklendi : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ogrencis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OgrenciAdi = c.String(),
                        OgrenciSoyadi = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Ogrencis");
        }
    }
}
