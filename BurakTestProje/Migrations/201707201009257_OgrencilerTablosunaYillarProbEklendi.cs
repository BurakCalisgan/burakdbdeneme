namespace BurakTestProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OgrencilerTablosunaYillarProbEklendi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ogrencis", "YilSayisi", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ogrencis", "YilSayisi");
        }
    }
}
